#include <endian.h>
#include <stdio.h>
#include <string.h>

#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "rocksdb/merge_operator.h"


using namespace rocksdb;

// protected by the Facebook License and Patent same as RocksDB
// see code @ https://github.com/facebook/rocksdb/blob/master/util/coding.h


  // -- Implementation of the functions declared above
inline void EncodeFixed32(char* buf, uint32_t value) {
#if __BYTE_ORDER == __LITTLE_ENDIAN
  memcpy(buf, &value, sizeof(value));
#else
  buf[0] = value & 0xff;
  buf[1] = (value >> 8) & 0xff;
  buf[2] = (value >> 16) & 0xff;
  buf[3] = (value >> 24) & 0xff;
#endif
}

inline void EncodeFixed64(char* buf, uint64_t value) {
#if __BYTE_ORDER == __LITTLE_ENDIAN
  memcpy(buf, &value, sizeof(value));
#else
  buf[0] = value & 0xff;
  buf[1] = (value >> 8) & 0xff;
  buf[2] = (value >> 16) & 0xff;
  buf[3] = (value >> 24) & 0xff;
  buf[4] = (value >> 32) & 0xff;
  buf[5] = (value >> 40) & 0xff;
  buf[6] = (value >> 48) & 0xff;
  buf[7] = (value >> 56) & 0xff;
#endif
}

// Pull the last 8 bits and cast it to a character
inline void PutFixed32(std::string* dst, uint32_t value) {
#if __BYTE_ORDER__ == __LITTLE_ENDIAN__
  dst->append(const_cast<const char*>(reinterpret_cast<char*>(&value)),
    sizeof(value));
#else
  char buf[sizeof(value)];
  EncodeFixed32(buf, value);
  dst->append(buf, sizeof(buf));
#endif
}

 inline void PutFixed64(std::string* dst, uint64_t value) {
#if __BYTE_ORDER__ == __LITTLE_ENDIAN__
   dst->append(const_cast<const char*>(reinterpret_cast<char*>(&value)),
               sizeof(value));
#else
   char buf[sizeof(value)];
   EncodeFixed64(buf, value);
   dst->append(buf, sizeof(buf));
#endif
 }

inline uint32_t DecodeFixed32(const char* ptr) {
#if __BYTE_ORDER__ == __LITTLE_ENDIAN__
  // Load the raw bytes
  uint32_t result;
  memcpy(&result, ptr, sizeof(result));  // gcc optimizes this to a plain load
  return result;
#else
  return ((static_cast<uint32_t>(static_cast<unsigned char>(ptr[0])))
          | (static_cast<uint32_t>(static_cast<unsigned char>(ptr[1])) << 8)
          | (static_cast<uint32_t>(static_cast<unsigned char>(ptr[2])) << 16)
          | (static_cast<uint32_t>(static_cast<unsigned char>(ptr[3])) << 24));
#endif
}


 inline uint64_t DecodeFixed64(const char* ptr) {
#if __BYTE_ORDER__ == __LITTLE_ENDIAN__
   // Load the raw bytes
   uint64_t result;
   memcpy(&result, ptr, sizeof(result));  // gcc optimizes this to a plain load
   return result;
#else
   uint64_t lo = DecodeFixed32(ptr);
   uint64_t hi = DecodeFixed32(ptr + 4);
   return (hi << 32) | lo;
#endif
 }


inline std::vector<uint64_t> DecodeVector (const Slice& value, Logger* logger, int vec_size) {

  std::vector<uint64_t> vec_result;
  int offset = sizeof(uint64_t);

  for (int i=0;i<vec_size;++i) {
    uint64_t val = 0;
    val = DecodeFixed64(value.data()+(i*offset));
    vec_result.push_back(val);
  }

  return vec_result;

}
