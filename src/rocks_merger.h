#include <assert.h>
#include <memory>
#include <iostream>
#include <cstdio>
#include <string>
#include <algorithm>

#include "rocks_serializer.h"

using namespace rocksdb;

class CountMergeOperator : public AssociativeMergeOperator {

 public:

  /* size_t num_merge_operator_calls; */
  /* size_t num_partial_merge_calls; */

  virtual bool Merge(const Slice& key,
                     const Slice* existing_value,
                     const Slice& value,
                     std::string* new_value,
                     Logger* logger) const override {

    std::vector<uint64_t> orig_vec;

    int vec_size = value.size()/sizeof(uint64_t);
    if (existing_value) {
      orig_vec = DecodeVector(*existing_value, logger, vec_size);
    } else {
      for (int i=0; i<vec_size;++i) {
        orig_vec.push_back((uint64_t)0);
      }
    }

    std::vector<uint64_t> operand = DecodeVector(value, logger, vec_size);

    assert(new_value);
    new_value->clear();

    // increment the old vector with the new one [0,1,0..]
    std::transform (orig_vec.begin(), orig_vec.end(), operand.begin(),
                    orig_vec.begin(), std::plus<uint64_t>());

    for (std::vector<uint64_t>::iterator it = orig_vec.begin();
         it != orig_vec.end(); ++it) {
      PutFixed64(new_value, *it);
    }

    return true;  // Return true always since corruption will be treated as 0

  }

  /* virtual bool PartialMergeMulti(const Slice& key, */
  /*                                const std::deque<Slice>& operand_list, */
  /*                                std::string* new_value, */
  /*                                Logger* logger) const override { */
  /*   assert(new_value->empty()); */
  /*   /\* ++num_partial_merge_calls; *\/ */
  /*   return mergeOperator_->PartialMergeMulti(key, operand_list, new_value, logger); */
  /* } */

  virtual const char* Name() const override {
    return "UInt64AddOperator";
  }

 private:

  std::shared_ptr<MergeOperator> mergeOperator_;

  uint64_t DecodeInteger(const Slice& value, Logger* logger) const {

    uint64_t result = 0;
    if (value.size() == sizeof(uint64_t)) {
      result = DecodeFixed64(value.data());
    } else if (logger != nullptr) {
      // If value is corrupted, treat it as 0
      /* Log(InfoLogLevel::ERROR_LEVEL, logger, */
      /*     "uint64 value corruption, size: %" ROCKSDB_PRIszt */
      /*     " > %" ROCKSDB_PRIszt, */
      /*     value.size(), sizeof(uint64_t)); */
    }

    return result;
  }

  /* std::vector<uint64_t> DecodeVector (const Slice& value, Logger* logger, int vec_size) const { */

  /*   std::vector<uint64_t> vec_result; */
  /*   int offset = sizeof(uint64_t); */

  /*   for (int i=0;i<vec_size;++i) { */
  /*     uint64_t val = 0; */
  /*     val = DecodeFixed64(value.data()+(i*offset)); */
  /*     vec_result.push_back(val); */
  /*   } */

  /*   return vec_result; */

  /* } */

};
