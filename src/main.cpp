#include <zlib.h>
#include <stdio.h>
#include <bzlib.h>
#include <thread>

#include <iostream>
#include <chrono>
#include <vector>

#include "masp_comparator.h"


int main(int argc, char *argv[]) {

  if (argc<3) {
    printf("Need at least 1 genome to compare !\n");
    return -1;
  }


  masp::Comparator kmer_comparator(argv+1,argc-1);
  kmer_comparator.StoreKmersCountOnRocks();
  vector< vector<uint64_t> > gram_matrix = kmer_comparator.CreateGramMatrixBinary();
  kmer_comparator.PrintGramMatrix(gram_matrix);


  return 0;

}
