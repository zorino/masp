#include <cstdio>
#include <string>
#include <iostream>

#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "rocksdb/merge_operator.h"


/* #include "rocks_serializer.h" */
#include "rocks_merger.h"

using namespace rocksdb;
using namespace std;


class Counters {

 public:

  uint64_t kDefaultCount = 0;

  explicit Counters(std::shared_ptr<DB> db, uint64_t defaultCount = 0)
    : db_(db){
  };


  // mapped to a RocksDB Put
  bool Set(const string& key, uint64_t value) {
    char serialized[sizeof(uint64_t)];
    EncodeFixed64(serialized, value);
    db_->Put(rocksdb::WriteOptions(), key,  serialized);
    return true;
  }

  // mapped to a RocksDB Delete
  void Remove(const string& key) {
    db_->Delete(rocksdb::WriteOptions(), key);
  }

  // mapped to a RocksDB Get
  /* bool Get(const string& key, uint64_t *value) { */
  bool Get(const string& key, std::vector<uint64_t> &vec_result) {

    rocksdb::Logger *logger;
    string str;
    auto s = db_->Get(rocksdb::ReadOptions(), key,  &str);

    int offset = sizeof(uint64_t);
    int vec_size = str.size()/offset;

    vec_result = DecodeVector(str,logger, vec_size);

    /* for (int i=0;i<vec_size;++i) { */
    /*   uint64_t val = 0; */
    /*   val = DecodeFixed64(&str[0]+(i*offset)); */
    /*   vec_result.push_back(val); */
    /* } */

    if (s.ok()) {
      return true;
    } else {
      std::cerr << s.ToString() << std::endl;
      return false;
    }

  }


  // implemented as get -> modify -> set
  bool Add(const string& key, std::vector<uint64_t> *value) {

    std::string serialized;

    for (std::vector<uint64_t>::iterator it = value->begin() ; it != value->end(); ++it) {
      PutFixed64(&serialized, *it);
    }

    uint64_t size = value->size()*sizeof(uint64_t);
    Slice slice(&serialized[0], size);

    auto s = db_->Merge(rocksdb::WriteOptions(), key, slice);

    if (s.ok()) {
      return true;
    } else {
      std::cerr << s.ToString() << std::endl;
      return false;
    }
  }


 private:

  std::shared_ptr<DB> db_;

};

