#include <thread>
#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <memory>

#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "rocksdb/merge_operator.h"


/* #include "rocks_counter.h" */

#include "ksequence.h"
#include "thread_pool.h"


namespace masp {

  class Comparator {

  public:

    Comparator(char **samples, int nb_samples);
    ~Comparator();

    void SetSampleList(char **samples);

    void StoreKmersCountOnRocks();

    vector< vector<uint64_t> > CreateGramMatrixBinary();
    void PrintGramMatrix(vector< vector<uint64_t> > &gram_matrix);

  private:

    char **m_samples;
    unsigned nb_of_samples;

    rocksdb::DB * db;
    rocksdb::Options options;
    rocksdb::Status status;
    std::shared_ptr<rocksdb::DB> dbp;
    void SetDB();

    /* struct thread_args { */
    /*   char * filename; */
    /*   std::shared_ptr<rocksdb::DB> dbp; */
    /*   int sample; */
    /*   int nb_sample; */
    /* }; */

    /* void ThreadWorker(struct thread_args arg); */

  };

}

