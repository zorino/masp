#include "masp_comparator.h"

using concurrent::ThreadPool;

masp::Comparator::Comparator(char **samples, int nb_samples) {

  m_samples = samples;
  nb_of_samples = nb_samples;
  SetDB();

}


masp::Comparator::~Comparator() {}


void masp::Comparator::SetDB () {

  // std::cout << "Setting DB" << std::endl;

  options.create_if_missing = true;
  options.IncreaseParallelism();
  options.OptimizeLevelStyleCompaction();
  options.merge_operator = std::make_shared<CountMergeOperator>();
  status = rocksdb::DB::Open(options, "./gram-matrix.rocksdb", &db);
  dbp = std::shared_ptr<rocksdb::DB>(db);

  // std::cout << "DB set" << std::endl;

}


struct thread_args {
  char * filename;
  std::shared_ptr<rocksdb::DB> dbp;
  int sample;
  int nb_sample;
};


void ThreadWorker(struct thread_args &arg) {

  // std::cout << arg.filename << std::endl;
  // std::cout << arg.sample << std::endl;
  std::cout << "Thread Start: " << arg.filename << std::endl;

  KSequence k(arg.filename, arg.sample, arg.nb_sample);
  k.RocksStoreKmerCount(31,arg.dbp);

  std::cout << "Thread Done: " << arg.filename << std::endl;

  return;

}


void masp::Comparator::StoreKmersCountOnRocks() {

  unsigned int threads_supported = std::thread::hardware_concurrency();

  std::cout << "Number of threads supported : " << threads_supported << std::endl;

  ThreadPool pool(threads_supported);

  for( int i = 0; i < nb_of_samples; ++i ) {

    struct thread_args args;
    args.filename = m_samples[i];
    args.dbp = dbp;
    args.sample = i+1;
    args.nb_sample = nb_of_samples;

    std::function<void()> task = std::bind(ThreadWorker,args);

    pool.AddJob(task);

  }

  pool.JoinAll();

  return;

}


vector< vector<uint64_t> > masp::Comparator::CreateGramMatrixBinary() {

  vector< vector<uint64_t> > gram_matrix;

  for(int i=0; i<nb_of_samples; ++i) {
    vector<uint64_t> v;
    for(int j=i; j<nb_of_samples;j++) {
      v.push_back((uint64_t)0);
    }
    gram_matrix.push_back(v);
  }

  rocksdb::Logger *logger;

  rocksdb::Iterator* it = dbp->NewIterator(rocksdb::ReadOptions());

  std::vector<uint32_t> indices;
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    // cout << it->key().ToString() << endl;
    std::vector<uint64_t> val = DecodeVector(it->value(), logger, nb_of_samples);

    for(std::vector<uint64_t>::iterator it=val.begin();it<val.end();++it) {
      if(*it!=0){
        indices.push_back(it-val.begin());
      }
    }

    for(int i=0; i<indices.size();++i) {
      for(int j=i; j<indices.size();++j) {
        gram_matrix[indices[i]][indices[j]-indices[i]] += 1;
      }
    }

    indices.clear();

  }

  assert(it->status().ok());

  delete it;

  return gram_matrix;

}


void masp::Comparator::PrintGramMatrix(vector< vector<uint64_t> > &gram_matrix) {

  ofstream myfile;
  myfile.open("gram_matrix_binary.tsv");

  std::cout << "##### Matrix" << std::endl;
  for(int i=0; i<nb_of_samples;++i) {
    std::cout << "sample_" << (i+1);
    myfile << m_samples[i];
    for(int j=0; j<nb_of_samples;++j) {
      if(i>j) {
        std::cout << "\t" << gram_matrix[j][i-j];
        myfile << "\t" << gram_matrix[j][i-j];
      } else {
        std::cout << "\t" << gram_matrix[i][j-i];
        myfile << "\t" << gram_matrix[i][j-i];
      }
    }
    std::cout << std::endl;
    myfile << std::endl;
  }

  std::cout << "##### End of Matrix" << std::endl;
  myfile.close();
  return;

}
