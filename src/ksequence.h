#include <zlib.h>
#include <stdio.h>
#include <memory>


#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "rocksdb/merge_operator.h"

#include "rocks_counter.h"

/* #include "rocks_miner.h" */
/* #include "rocks_counter.h" */


class KSequence {

 public:

  KSequence(char *f, int sample, int nb_samples);
  ~KSequence();

  void RocksStoreKmerCount(int length, std::shared_ptr<rocksdb::DB> dbp);
  /* void RocksStoreKmerCount(int length); */
  void PrintSequenceInformation();

 private:

  char *filename;
  int m_sample;
  int m_nb_samples;

  bool CanonicalKmer(std::string &raw_kmer, std::string &kmer_key);
  char NtComplement(char n);

};
