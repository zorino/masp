#include <sstream>
#include <string>
#include <iostream>

#include "kseq.h"
#include "ksequence.h"


KSEQ_INIT(gzFile, gzread)
//KSEQ_INIT(gzFile, BZ2_bzread)   // for BZ2 compressed file


KSequence::KSequence(char *f, int sample, int nb_samples) {

  filename = f;
  m_sample = sample;
  m_nb_samples = nb_samples;

}

KSequence::~KSequence() {}


void KSequence::RocksStoreKmerCount(int length, std::shared_ptr<rocksdb::DB> dbp) {

  gzFile fp;
  kseq_t *seq;
  int l,k;
  char *buffer;
  std::stringstream ss;
  std::string kmer;
  std::string kmer_key;

  Counters counter(dbp,0);

  fp = gzopen(filename, "r");

  seq = kseq_init(fp);

  // std::cout << "Reading Sequences" << std::endl;

  // iterate over each sequence
  while((l = kseq_read(seq)) >= 0) {

    // printf("#Name:%s|Length:%d\n", seq->name.s, seq->seq.l);
    k = 0;
    // iterage over each kmer in the sequence
    while((k+length+1) < seq->seq.l) {

      buffer = seq->seq.s+k;
      ss.str("");
      ss.clear();
      for (int i=0; i<length; i++) {
        // std::cout << buffer[i];
        ss << buffer[i];
      }
      ss >> kmer;


      // std::cout << std::endl << kmer << std::endl;
      // kminer.MineKmer(kmer);

      // uint64_t val = 1;
      std::vector<uint64_t> vec;
      for(int i=0;i<m_nb_samples;++i) {
        vec.push_back((uint64_t)0);
      }

      vec.at(m_sample-1) = (uint64_t)1;

      if (CanonicalKmer(kmer,kmer_key)) {
        counter.Add(kmer_key, &vec);

        // std::vector<uint64_t> test_val;
        // counter.Get(kmer_key, test_val);
        // std::cout << "Counter : ";
        // for(std::vector<uint64_t>::iterator it = test_val.begin(); it!=test_val.end(); ++it) {
        //   std::cout << *it << ",";
        // }
        // std::cout << std::endl;

      }

      k += 1;

      // if ((k%1000)==0) {
      //   std::cout << "Flushing db" << std::endl;
      //   dbp->Flush(rocksdb::FlushOptions());
      //   dbp->CompactRange(rocksdb::CompactRangeOptions(), nullptr, nullptr);
      // }

    }
  }

  // std::cout << "Finished File : " << filename << std::endl;

  gzclose(fp);

}


void KSequence::PrintSequenceInformation() {

  gzFile fp;
  kseq_t *seq;
  int l;

  fp = gzopen(filename, "r"); // STEP 2: open the file handler

  // int bzError;
  // FILE *bz2File = fopen(argv[1], "r");
  // const int BLOCK_MULTIPLIER = 7;
  // fp = BZ2_bzReadOpen(&bzError, bz2File, 0, 0, NULL, 0);

  seq = kseq_init(fp); // STEP 3: initialize seq

  while ((l = kseq_read(seq)) >= 0) { // STEP 4: read sequence
    printf("name: %s\n", seq->name.s);
    if (seq->comment.l) printf("comment: %s\n", seq->comment.s);
    // printf("seq: %s\n", seq->seq.s);
    if (seq->qual.l) printf("qual: %s\n", seq->qual.s);
  }
  printf("return value: %d\n", l);
  kseq_destroy(seq); // STEP 5: destroy seq

  gzclose(fp); // STEP 6: close the file handler

  // BZ2_bzReadClose(&bzError, bz2File);

}

bool KSequence::CanonicalKmer (std::string &raw_kmer, std::string &kmer_key) {

  std::string reverse_kmer;
  for (auto & c: raw_kmer) c = toupper(c);
  for (std::string::reverse_iterator rit=raw_kmer.rbegin(); rit != raw_kmer.rend(); ++rit) {
    if(*rit=='N') {
      // std::cout << "Not a kmer !! " << std::endl;
      return false;
    }
    reverse_kmer.push_back(NtComplement(*rit));
  }

  if(reverse_kmer < raw_kmer){
    kmer_key = reverse_kmer;
  } else {
    kmer_key = raw_kmer;
  }

  return true;

}


char KSequence::NtComplement(char n) {

  switch(n) {
  case 'A':
    return 'T';
  case 'T':
    return 'A';
  case 'G':
    return 'C';
  case 'C':
    return 'G';
  }
  std::cout << n ;
  // assert(false);
  return 'N';

}
