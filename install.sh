#!/bin/bash

rocksdb_v=5.7.3
lz4_v=1.8.0
snappy_v=1.1.7

function install_lz4 () {
    wget https://github.com/lz4/lz4/archive/v$lz4_v.tar.gz
    tar xvf v$lz4_v.tar.gz
    rm v$lz4_v.tar.gz
    ln -s lz4-$lz4_v lz4
    cd lz4
    make
    cd ../
}

function install_snappy () {
    wget https://github.com/google/snappy/archive/$snappy_v.tar.gz
    tar xvf $snappy_v.tar.gz
    rm $snappy_v.tar.gz
    ln -s snappy-$snappy_v snappy
    cd snappy
    mkdir build
    cd build && cmake ../ && make
    cd ../../
}

function install_rocksdb () {
    wget https://github.com/facebook/rocksdb/archive/v$rocksdb_v.tar.gz
    tar xvf v$rocksdb_v.tar.gz
    rm v$rocksdb_v.tar.gz
    ln -s rocksdb-5.7.3 rocksdb
    cd rocksdb
    make static_lib
    cd ../
}

if ! test -d ext_libs
then
   mkdir ext_libs
   cd ext_libs
fi

echo "Installing MASP dependencies.."
install_lz4
install_rocksdb
