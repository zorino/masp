# MASP README

Early stage development of a genomic k-mers library based on rocksdb 

## Installation
```
git clone https://zorino@bitbucket.org/zorino/masp.git --recursive
./install.sh
cd masp/src
make
```

## Execution
```
./masp *.fasta
 
```

## Library
* kmer.h (http://lh3lh3.users.sourceforge.net/kseq.shtml)
* rocksdb (https://github.com/facebook/rocksdb)

